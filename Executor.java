public class Executor {

	public static void main(String[] args) {
		Utility.init(); // initializes file readers
		String[] questions = Utility.readQuestions(); //reads question.txt file into questions array
		String[] answers = Utility.readAnswers(); // reads answers.txt file into answers array
		
		int numOracles = answers.length; //finds the number of oracles
		
		//TODO Assign questions to oracles using Utility.random and print the question paired with the oracle response

		// 1. Initialize one ArrayQueue per oracle (arrays will work best).
		ArrayQueue[] oracles = new ArrayQueue[numOracles];
		for(int i = 0; i < numOracles; i++){
			//give each oracle an ArrayQueue
			oracles[i] = new ArrayQueue(questions.length);
		}

		// 2. Put the questions into the queues, assigning each one to the queue of the oracle whose number is returned by the random number generator.
		for(int i = 0; i < questions.length; i++){
			int rand = Utility.random(numOracles);
			//give the randomly chosen oracle a question
			oracles[rand].enqueue(questions[i]);
		}

		// 3. Loop through the oracles, having each one remove a question from its queue (if empty do nothing) and answer it with its unique answer (oracle[k] uses answers[k]). Do this repeatedly till all queues become empty.
		int questionsAnswered = 0; //track total questions answered
		do{
			for(int i = 0; i < numOracles; i++){
				//if the oracle has a question...
				if(!oracles[i].isEmpty()){
					System.out.println(oracles[i].dequeue() + ": " + answers[i]);
					questionsAnswered++; //increment question count when question is answered
				}
			}
		}while(questionsAnswered < questions.length); //when all questions have been answered stop looping
	}
}