
public class ArrayQueue {
	private String[] queueArray;
	private int size;
	private int front;
	private int back;
	
	public ArrayQueue(){
		queueArray = new String[100];
		size = 0;
		front = 0;
		back = -1;
	}
	
	public ArrayQueue(int startSize){
		queueArray = new String[startSize];
		size = 0;
		front = 0;
		back = -1;
	}
	/**
	 * @function returns the number of elements in the queue
	 * @return size
	 */
	public int getSize(){
		return size;
	}
	/**
	 * @function adds a string to the end of the queue
	 * @param toEnqueue: the input to be inserted
	 */
	public void enqueue(String toEnqueue){
		//only queue if the array is not full
		if(size >= queueArray.length){
			System.out.println("Queue already full");
		}else{
			//queue the input
			int next = (back + 1) % queueArray.length; //loop to the beginning of the array if the end has been reached
			queueArray[next] = toEnqueue;
			back = next;
			size++;
		}
	}
	
	/**
	 * @function removes the string from the front of the queue
	 * @return the string from the front of the queue
	 * returns null if the queue is empty
	 */
	public String dequeue(){
		String toDequeue = null; //set to null premptively
		if(!this.isEmpty()){
			toDequeue = queueArray[front];
			front = (front + 1) % queueArray.length; //loop to the beginning of the array if the end has been reached
			size--;
		}
		return toDequeue;
	}
	
	/**
	 * 
	 * @return returns true if the queue is empty, false otherwise
	 */
	public boolean isEmpty(){
		return size <= 0;
	}

	/**
	 * 
	 * @return returns true if the queue is full, false otherwise
	 */
	public boolean isFull(){
		return size >= queueArray.length;
	}
	
}
